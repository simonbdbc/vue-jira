import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  created () {
    require.context("./../modules", true, /\.(js|vue)$/i);
    // const reqStore = require.context("./../modules", true, /\.(js)$/i);

    // // reqStore.keys().map(key => {
    // //   const store = key.match(/store.js/g);
    // //   if (store !== null) {
    // //     const moduleName = key.split("/")[1];
    // //     this.$store.registerModule(moduleName, reqStore(key).default);
    // //   }
    // // });
  },
  render: h => h(App)
}).$mount('#app')
