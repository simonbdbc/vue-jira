import Dashboard from './views/Dashboard.vue'

const route = [
  {
    path: '/',
    name: 'dashboard',
    component: Dashboard,
    meta: {
      layout: 'blank'
    }
  }
]

export default route